import {TestBed} from '@angular/core/testing';

import {DelayService} from './delay.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';

describe('DelayService', () => {
  let service: DelayService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule, HttpClientTestingModule, FormsModule, ScrollingModule,
        BrowserAnimationsModule, MatFormFieldModule, MatInputModule, MatIconModule, ReactiveFormsModule]
    })
      .compileComponents();
    service = TestBed.inject(DelayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
