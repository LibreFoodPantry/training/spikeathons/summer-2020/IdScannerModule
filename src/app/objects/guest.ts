export class Guest {
  studentID: string;
  remainingWeight: number;
  totalWeight: number;

  constructor(studId: string, remWeight: number, totWeight: number) {
    this.studentID = studId;
    this.remainingWeight = remWeight;
    this.totalWeight = totWeight;
  }
}
