import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IdScannerComponent} from './id-scanner.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {SvgContainerComponent} from '../svg-container/svg-container.component';

describe('IdScannerComponent', () => {
  let component: IdScannerComponent;
  let fixture: ComponentFixture<IdScannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IdScannerComponent, SvgContainerComponent],
      imports: [MatSnackBarModule, HttpClientTestingModule, FormsModule, ScrollingModule,
        BrowserAnimationsModule, MatFormFieldModule, MatInputModule, MatIconModule, ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdScannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onKey with events', () => {
    const spy = spyOn(component, 'onKey');
    const id = '7654321';
    const entry = ';0000' + id + '00000';
    for (let s of entry) {
      let eventMock: KeyboardEvent = new KeyboardEvent('keypress', {
        'key': s
      });
      component.onKey(eventMock);
      expect(spy).toHaveBeenCalledWith(eventMock);
    }

    let eventMock: KeyboardEvent = new KeyboardEvent('keypress', {
      'key': 'Shift'
    });
    component.onKey(eventMock);
    expect(spy).toHaveBeenCalledWith(eventMock);

    eventMock = new KeyboardEvent('keypress', {
      'key': 'Enter'
    });
    component.onKey(eventMock);
    expect(spy).toHaveBeenCalledWith(eventMock);
  });

  it('should update id after keypress', () => {
    const id = '7654321';
    const triggerChar = component.SCANNER_TRIGGER_KEY;
    const startPattern = component.SCANNER_START_PATTERN;
    const extraCharacters = '00000';
    const entry = triggerChar + startPattern + id + extraCharacters;
    for (let s of entry) {
      let eventMock: KeyboardEvent = new KeyboardEvent('keypress', {
        'key': s
      });
      component.onKey(eventMock);
    }
    component.onKey(new KeyboardEvent('keypress', {'key': component.SCANNER_END_KEY}));
    component.onKey(new KeyboardEvent('keypress', {'key': 'Enter'}));
    expect(component.getId()).toBe(id);
  });

  it('should have no id after incorrect start pattern', () => {
    const id = '7654321';
    const triggerChar = component.SCANNER_TRIGGER_KEY;
    const startPattern = '9876';
    const extraCharacters = '00000';
    const entry = triggerChar + startPattern + id + extraCharacters;
    for (let s of entry) {
      let eventMock: KeyboardEvent = new KeyboardEvent('keypress', {
        'key': s
      });
      component.onKey(eventMock);
    }
    component.onKey(new KeyboardEvent('keypress', {'key': component.SCANNER_END_KEY}));
    component.onKey(new KeyboardEvent('keypress', {'key': 'Enter'}));
    expect(component.getId()).toBe('');
  });

  it('should have no id after escape', () => {
    const id = '7654321';
    const triggerChar = component.SCANNER_TRIGGER_KEY;
    const startPattern = '9876';
    const extraCharacters = '00000';
    const entry = triggerChar + startPattern + id + extraCharacters;
    for (let s of entry) {
      let eventMock: KeyboardEvent = new KeyboardEvent('keypress', {
        'key': s
      });
      component.onKey(eventMock);
    }
    component.onKey(new KeyboardEvent('keypress', {'key': 'Escape'}));
    component.onKey(new KeyboardEvent('keypress', {'key': 'Enter'}));
    expect(component.getId()).toBe('');
  });

  it('should parse and update text input', () => {
    const id = '7654321';
    const startPattern = component.SCANNER_START_PATTERN;
    const extraCharacters = '00000';
    const entry = startPattern + id + extraCharacters;

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const input = fixture.debugElement.query(By.css('input')).nativeElement;
      input.value = entry;
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        component.onInputKeyUpEnter();
        expect(component.getId()).toBe(id);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(input.value).toBe(id);
        });
      });
    });
  });

  it('should submit raw input if start pattern incorrect', () => {
    const id = '7654321';
    const startPattern = '9876';
    const extraCharacters = '00000';
    const entry = startPattern + id + extraCharacters;

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const input = fixture.debugElement.query(By.css('input')).nativeElement;
      input.value = entry;
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        component.onInputKeyUpEnter();
        expect(component.getId()).toBe(entry);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(input.value).toBe(entry);
        });
      });
    });
  });
});
