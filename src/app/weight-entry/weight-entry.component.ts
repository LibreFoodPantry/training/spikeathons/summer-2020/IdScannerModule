import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DelayService} from '../services/delay-service/delay.service';
import {NewGuestService} from '../services/new-guest-service/new-guest.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-weight-entry',
  templateUrl: './weight-entry.component.html',
  styleUrls: ['./weight-entry.component.css'],
  providers: [ DelayService ]
})
export class WeightEntryComponent implements OnInit {
  inputWeight: number;
  maxWeight: number;
  entryMessage: boolean = true;
  guestID: string;
  @Output() closeContainerEmitter = new EventEmitter<boolean>();
  @Output() clearGuestEmitter = new EventEmitter();


  weightControl = new FormControl('');


  constructor(private delayService: DelayService, private newGuestService: NewGuestService){ }

  ngOnInit(): void {
    this.guestID = this.newGuestService.currGuestID;
    this.maxWeight = this.newGuestService.currGuestRem;
    this.weightControl.setValidators([Validators.required, Validators.pattern(/^['0-9']*$/), Validators.max(this.maxWeight)]);

    this.entryMessage = true;
    this.delayService.delay(400).then(() => {
      this.focusOnTextEntry();
    });
  }

  @ViewChild("focusMe") nameField: ElementRef;
  focusOnTextEntry(): void {
    this.nameField.nativeElement.focus();
  }

  getErrorMessage() {
    if(this.weightControl.hasError('max')){
      return 'Weight is above remaining limit';
    }
    if(this.weightControl.hasError('pattern')) {
      return 'Only numbers are allowed';
    }
    return this.weightControl.hasError('required') ? 'You must enter a value' : '';
  }

  hideEntryMessage(): void {
    this.delayService.delay(250).then(() => {
      this.entryMessage = false;
    })
  }

  submitTransaction(): void {
    this.newGuestService.logTransaction(this.guestID, this.inputWeight);
    this.hideEntryMessage();
    this.delayService.delay(1000).then(() => {
      this.closeContainer();
      this.clearGuest();
    })
  }

  closeContainer(): void {
    this.closeContainerEmitter.emit(false);
  }

  clearGuest(): void {
    this.clearGuestEmitter.emit();
  }

}
