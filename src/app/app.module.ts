import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {IdScannerComponent} from './id-scanner/id-scanner.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ClickOutsideDirective} from './directives/click-outside.directive';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {HttpClientModule} from '@angular/common/http';
import { WeightEntryComponent } from './weight-entry/weight-entry.component';
import {SvgContainerComponent} from './svg-container/svg-container.component';
import { DuplicateIdPopupComponent } from './duplicate-id-popup/duplicate-id-popup.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    IdScannerComponent,
    ClickOutsideDirective,
    WeightEntryComponent,
    SvgContainerComponent,
    DuplicateIdPopupComponent,
  ],
    imports: [
      BrowserModule,
      MatButtonModule,
      MatButtonToggleModule,
      BrowserAnimationsModule,
      ScrollingModule,
      FormsModule,
      MatInputModule,
      MatIconModule,
      HttpClientModule,
      ReactiveFormsModule,
      MatSnackBarModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
